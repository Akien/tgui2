TGUI2 is a GUI library that uses Allegro 5 and is used in Crystal Picnic, Monster RPG 2 (an older version than this repo), AshEdit and others.

There are a few widgets included, but it's mainly intended to be used to create your own widgets.

TGUI2 should be considered "legacy" code. Fixes are the only likely candidates for changes, mainly to support Crystal Picnic and AshEdit. TGUI3 has been released and is preferred.
